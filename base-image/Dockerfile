FROM ubuntu:16.10
MAINTAINER joezuntz@googlemail.com
#Joe's note to himself.  Compile this with: docker build -t joezuntz/cosmosis-base .
#then docker push joezuntz/cosmosis-base

# Basic compilers and tools dependencies
RUN apt-get update -y && apt-get install -y gcc g++ gfortran wget make python-dev \
    pkg-config curl \
    && apt-get clean all

# Manual installation of mpich seems to be required to work on NERSC
RUN mkdir /opt/mpich && cd /opt/mpich \
    && wget http://www.mpich.org/static/downloads/3.2/mpich-3.2.tar.gz \
    && tar xvzf mpich-3.2.tar.gz &&  cd mpich-3.2 && ./configure && make -j4 \
    && make install && rm -rf /opt/mpich

# other dependencies, some for matplotlib
RUN apt-get update -y && \
	apt-get install -y  libcfitsio-dev  libfftw3-dev   git  libgsl-dev \
    python-pip emacs-nox  libpng16-16 libxft2 libpangoft2-1.0-0 \
    libfreetype6-dev libhdf5-dev \
    && apt-get clean all

# Also need a manual install of mpi4py so that it uses the right libraries - pip seems not to work
RUN mkdir /opt/mpi4py && cd /opt/mpi4py \
&& wget https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-2.0.0.tar.gz \
&& tar -zxvf mpi4py-2.0.0.tar.gz && cd mpi4py-2.0.0 && python setup.py install \
&& rm -rf /opt/mpi4py


# Minuit2 optimizer installation - not available in apt-get except via ROOT which is massive.
RUN mkdir /opt/minuit && cd /opt/minuit && \
    wget http://project-mathlibs.web.cern.ch/project-mathlibs/sw/5_34_14/Minuit2/Minuit2-5.34.14.tar.gz \
    && tar -zxf Minuit2-5.34.14.tar.gz && cd Minuit2-5.34.14 && \
    ./configure --disable-openmp  && make && make install && cd .. && rm -rf  /opt/minuit


# The environment variables needed by the CosmoSIS build and runtime.
ENV COSMOSIS_SRC_DIR /cosmosis
ENV GSL_INC /usr/include
ENV GSL_LIB /usr/lib/x86_64-linux-gnu
ENV CFITSIO_INC /usr/include
ENV CFITSIO_LIB /usr/lib/x86_64-linux-gnu
ENV FFTW_LIBRARY /usr/lib/x86_64-linux-gnu
ENV FFTW_INC_DIR /usr/include
ENV MINUIT2_LIB /usr/local/lib
ENV MINUIT2_INC /usr/local/include
ENV LAPACK_LINK -L/usr/local/lib -lopenblas
ENV MINUIT2_LIB /usr/local/lib
ENV MINUIT2_INC /usr/local/include
ENV LD_LIBRARY_PATH ${COSMOSIS_SRC_DIR}/cosmosis/datablock:${COSMOSIS_SRC_DIR}/cosmosis-standard-library/likelihood/planck/plc-1.0/lib/:${COSMOSIS_SRC_DIR}/cosmosis-standard-library/likelihood/planck2015/plc-2.0/lib/:/usr/lib/x86_64-linux-gnu:/usr/local/lib:${LD_LIBRARY_PATH}
ENV PYTHONPATH ${COSMOSIS_SRC_DIR}:${PYTHONPATH}
ENV PATH ${COSMOSIS_SRC_DIR}/bin:${PATH}
ENV OMP_NUM_THREADS=1

# Run a bash login shell if no other command is specified.
CMD ["/bin/bash", "-l"]
