# This Dockerfile is a bit like a Makefile, except
# that it contains the recipe to build a Docker Image
# (like a virtual machine)

# The parent or base image contains many but not all of the cosmosis
# dependencies environment variables.  See the Dockerfile in base-image
# for how that was created originally (it will be downloaded from 
# hub.docker.io).

FROM joezuntz/cosmosis-base



# OpenBLAS linear algebra installation (provides lapack and blas)
# We could put this in the parent image (joezuntz/cosmosis-test-3) but then
# it would not be tuned as well to your machine.

RUN cd /opt && git clone https://github.com/xianyi/OpenBLAS && cd OpenBLAS && make && make PREFIX=/usr/local install && rm -rf /opt/OpenBLAS

# Many of the python dependencies themselves ultimately depend on LAPACK
# so we need to install them manually here
RUN pip install numpy scipy matplotlib nose pyyaml emcee kombine h5py astropy sklearn cosmolopy


# If you need more libraries or other 
# tools you can put the commands to install them in here, by 
# prefixing the install commands with "RUN".
# Here are some examples:

# You can install with pip:
RUN pip install ipython


# Or with the apt-get command from ubuntu. You can run the VM and search
# inside for what is available like this:
# ./start-cosmosis-vm
# apt-cache search name_to_search_for
#
RUN apt-get install -y cython

# For more involved multi-command installations have a look at the OpenBLAS example
# above that uses && to separate the commands.

# This allows continuous bash history  (so you can press up to get previous commands)
# even when re-launching the machine
ENV HISTFILE /cosmosis/.bash_history

# Install the FITSIO library - this is a pain to do with pip
RUN cd /opt && git clone https://github.com/esheldon/fitsio && cd fitsio && python setup.py build_ext   --use-system-fitsio install && rm -rf /opt/fitsio

# Here you can add any of your own dependencies/libraries/etc.
